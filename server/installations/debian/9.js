const shell = require('shelljs');


class Installer {
    constructor(sudo) {
        this.sudo = sudo;
        this.install();
    }

    exec(command, sudo = this.sudo) {
        shell.exec(sudo + command);
    }

    install() {
        this.exec('apt update');
        this.exec('apt install apt-transport-https dirmngr -y');
        this.exec('apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF');
        this.exec('echo "deb https://download.mono-project.com/repo/debian stable-stretch main" | tee /etc/apt/sources.list.d/mono-official-stable.list');
        this.exec('apt update');
        this.exec('apt install mono-devel -y');
        this.exec('apt install libopus-dev ffmpeg -y');
    }

}


module.exports = {Installer};