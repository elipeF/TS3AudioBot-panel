const exist = require('path-exists');
const _ = require('lodash');
const pkgDir = require('pkg-dir');


const possibilites = {
    debian:
        {9: `/debian/9.js`}
};

const checkInstallation = async () => {
    try {
        return await exist( await pkgDir(__dirname) + '/.installed')
    } catch (e) {
        throw new Error('Nie moge sprawdzic instalacji. Sprawdz chmody!');
    }

};

const findInstallSchema = (system, version) => {
    return _.has(possibilites[system], version);

};

const runInstallation = (system, version, sudo) => {
    const {Installer} = require(`/${system}/${version}.js`);
    new Installer(sudo);
};


module.exports = {checkInstallation, findInstallSchema, runInstallation};