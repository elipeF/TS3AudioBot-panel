const Sequelize = require('sequelize');
const message = require('./../models/messages');


const database = new Sequelize({
    // sqlite! now!
    dialect: 'sqlite',

    // the storage engine for sqlite
    // - default ':memory:'
    logging: false,
    operatorsAliases: false,
    storage: './panel.sqlite'
});

database.conTest = async () => {

    try {
        await database.authenticate();
        message.infoMessage('Ustanowiono polaczenie z baza');
    } catch (e) {
        message.errorMessage('Nie moge polaczyc sie z baza');
    }

};

module.exports = {database};