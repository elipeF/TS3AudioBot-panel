const express = require('express');
const morgan = require('morgan');
const path = require('path');
const fs = require('fs');
const bodyParser = require('body-parser');
const clear = require('clear');
const yargs = require('yargs');
const lsbRelease = require('bizzby-lsb-release');
const _ = require('lodash');
const Confirm = require('prompt-confirm');
const detect = require('detect-port');


const {database} = require('./db/sqlite');
const {running} = require('./middlewares/running');
const {exists} = require('./middlewares/exist');
const message = require('./models/messages');
const install = require('./installations/install');
const bot = require('./models/bot');

const sudoOptions = {
    describe: 'If installator needs a sudo permission. Set as true',
    boolean: true,
    demand: false,
};
const systemOptions = {
    describe: 'Distribution of system',
    demand: false,
    string: true,
};
const releaseOptions = {
    describe: 'Version of system',
    demand: false,
    number: true
};

const argv = yargs
    .command('install', 'Install all packages', {
        sudo: sudoOptions,
        system: systemOptions,
        release: releaseOptions
    })
    .help()
    .alias('help', 'h').argv;


//clear();
message.init();

install.checkInstallation().then((status) => {
    if (status) {
        const app = express();
        const accessLogStream = fs.createWriteStream(path.join(__dirname, '../access.log'), {flags: 'a'});

        app.use(morgan('tiny', {
            stream: accessLogStream,
            skip: function (req, res) {
                return res.statusCode < 400
            }
        }));

        app.use(bodyParser.urlencoded({
            extended: true
        }));
        app.use(bodyParser.json());

        app.post('/api/v1/bots', async (req, res) => {
            try {
                const body = await _.pick(req.body, ['ipAddress', 'port', 'password', 'nick', 'webApiPort']);
                const newBot = await bot.fs.create(body);
                res.send({bot: newBot});
            } catch (e) {
                res.status(400).send(e);
            }
        });

        app.get('/api/v1/bots', async (req, res) => {
            try {
                res.send({bots: await bot.db.findAllBots()});
            } catch (e) {
                res.status(400).send(e);
            }
        });

        app.delete('/api/v1/bots', async (req, res) => {
            try {
                const body = await _.pick(req.body, ['id']);
                await bot.fs.destroy(body.id);
                res.send();
            } catch (e) {
                res.status(400).send();
                message.errorMessage(e);
            }
        });

        app.get('/api/v1/bots/:id', exists, async (req, res) => {
            const id = req.params.id;
            try {
                res.send({bot: await bot.db.findOneBots(id)});
            } catch (e) {
                console.log(e);
                res.status(400).send(e);
            }
        });

        app.get('/api/v1/bots/:id/start', exists, async (req, res) => {
            const id = req.params.id;
            try {
                const bot = await bot.process.running(id, req.bot.data.port, req.bot.data.pid);
                if (!bot.pid) {
                    console.log(req.bot.data.dir);
                    await bot.process.start(id, req.bot.data.dir);
                } else {
                    console.log('test',bot);
                }
                res.status(400).send('Bot juz uruchomiony');
            } catch (e) {
                try {
                    await bot.process.start(id, req.bot.data.dir);
                    res.send();
                } catch (e) {
                    console.log(e);
                    res.status(400).send('Blad podczas startu!');
                }
            }
        });


        app.get('/api/v1/bots/:id/stop', exists, running, async (req, res) => {
            const id = req.params.id;
            try {
                await bot.process.stop(id, req.bot.process.port);
                res.send();
            } catch (e) {
                res.status(400).send('Blad podczas zatrzymywania!');
            }
        });

        app.get('/api/v1/bots/:id/restart', exists, async (req, res) => {
            const id = req.params.id;
            try {
                req.bot.process = await bot.process.running(id, req.bot.data.port, req.bot.data.pid);
                await bot.api.restart(req.bot.process.port);
                res.send();
            } catch (e) {
                try {
                    await bot.process.start(id, req.bot.data.dir);
                    res.send();
                } catch (e) {
                    res.status(400).send('Blad podczas startu!');
                }
            }
        });

        app.get('/api/v1/bots/:id/restart/hard', exists, async (req, res) => {
            const id = req.params.id;
            try {
                await bot.process.running(id, req.bot.data.port, req.bot.data.pid);
                await bot.process.restart(id, req.bot.data.dir, req.bot.data.port);
                res.send();
            } catch (e) {
                try {
                    await bot.process.start(id, req.bot.data.dir);
                    res.send();
                } catch (e) {
                    res.status(400).send('Blad podczas startu!');
                }
            }
        });

        app.get('/api/v1/bots/:id/users', exists, async (req, res) => {
            try {
                const users = await bot.fs.usersList(req.bot.data.dir);
                res.send({users});
            } catch (e) {
                res.status(400).send();
            }
        });

        app.post('/api/v1/bots/:id/users', exists, running, async (req, res) => {
            try {
                const body = await _.pick(req.body, ['userUniqueId']);
                const users = await bot.fs.usersSave(req.bot.data.dir, body.userUniqueId);
                await bot.api.reload(req.bot.data.port);
                res.send({users});
            } catch (e) {
                res.status(400).send();
            }
        });

        app.delete('/api/v1/bots/:id/users', exists, running, async (req, res) => {
            try {
                const body = await _.pick(req.body, ['userUniqueId']);
                const users = await bot.fs.usersRemove(req.bot.data.dir, body.userUniqueId);
                await bot.api.reload(req.bot.data.port);
                res.send({users});
            } catch (e) {
                console.log(e);
                res.status(400).send();
            }
        });

        app.get('/api/v1/bots/:id/status', exists, async (req, res) => {
                const id = req.params.id;
                try {
                    req.bot.process = await bot.process.running(id, req.bot.data.port, req.bot.data.pid);
                    if (req.bot.process.status === 3) {
                        res.send({status: 'Online'})
                    } else if (req.bot.process.status === 2) {
                        res.send({status: 'Disconnected'})
                    } else if (req.bot.process.status === 1) {
                        res.send({status: 'Offline'});
                    } else {
                        res.send({status: 'Offline'});
                    }
                } catch (e) {
                    res.send({status: 'Offline'});
                }
            }
        );


        app.get('/api/v1/bots/:id/settings/name', exists, running, async (req, res) => {
            try {
                const name = await bot.api.getName(req.bot.data.id, req.bot.data.port);
                res.send({name});
            } catch (e) {
                console.log(e);
                res.status(400).send();
            }
        });

        app.post('/api/v1/bots/:id/settings/name', exists, running, async (req, res) => {
            try {
                const body = await _.pick(req.body, ['name']);
                const name = await bot.api.setName(req.bot.data.id, req.bot.data.port, body.name);
                res.send({name});
            } catch (e) {
                console.log(e);
                res.status(400).send();
            }
        });


        app.get('/api/v1/bots/:id/settings/channel', exists, running, async (req, res) => {
            try {
                const cid = await bot.api.getChannel(req.bot.data.id, req.bot.data.port);
                res.send({cid});
            } catch (e) {
                console.log(e);
                res.status(400).send();
            }
        });

        app.post('/api/v1/bots/:id/settings/channel', exists, running, async (req, res) => {
            try {
                const body = await _.pick(req.body, ['cid']);
                const cid = await bot.api.setChannel(req.bot.data.id, req.bot.data.port, body.cid);
                res.send({cid});
            } catch (e) {
                console.log(e);
                res.status(400).send();
            }
        });

        app.get('/api/v1/bots/:id/settings/youtube-dl', exists, running, async (req, res) => {
            try {
                const ytdl = await bot.api.getYoutubeDl(req.bot.data.port);
                res.send({enabled: ytdl});
            } catch (e) {
                console.log(e);
                res.status(400).send();
            }
        });

        app.post('/api/v1/bots/:id/settings/youtube-dl', exists, running, async (req, res) => {
            try {
                await bot.api.setYoutubeDl(req.bot.data.port);
                res.send({status: true});
            } catch (e) {
                console.log(e);
                res.status(400).send();
            }
        });

        app.delete('/api/v1/bots/:id/settings/youtube-dl', exists, running, async (req, res) => {
            try {
                await bot.api.delYoutubeDl(req.bot.data.port);
                res.send({status: true});
            } catch (e) {
                console.log(e);
                res.status(400).send();
            }
        });

        app.get('/api/v1/version', async (req, res) => {
            try {
                res.send({version: require('./version').version})
            } catch (e) {
                console.log(e);
                res.status(400).send();
            }
        });

        detect(8080).then(_port => {
            if (8080 === _port) {
                app.listen(8080, async () => {
                    try {
                        await database.conTest();
                        await bot.checkBotInstallPath();
                        message.infoMessage('Aplikacja uruchomiona!');
                        require('./models/schedule');
                    } catch (e) {
                        message.errorMessage('Nie moge wystartowac aplikacji!' + e);
                    }
                });
            } else {
                message.errorMessage('Nie moge wystartowac aplikacji!');
            }
        })
            .catch(() => {
                message.errorMessage('Nie moge wystartowac aplikacji!');
            });


    } else {

        if (argv._[0] === 'install' || argv._[0] === 'i') {

            if (argv.system || argv.release) {
                if (argv.system && argv.release) {
                    const system = {distribution: argv.system, release: argv.release.toString()};
                    if (_.isUndefined(argv.sudo)) {
                        system.sudo = '';
                    } else {
                        system.sudo = 'sudo ';
                    }
                    if (install.findInstallSchema(system.distribution.toLowerCase(), system.release.split('.')[0])) {
                        message.warningMessage(`System: ${system.distribution} w wersji: ${system.release.split('.')[0]}`);
                        new Confirm('Rozpoczac instalacje?')
                            .ask(function (answer) {
                                if (answer) {
                                    message.infoMessage('Rozpoczynam instalacje!');
                                    install.runInstallation(system.distribution.toLowerCase(), system.release.split('.')[0], system.sudo)
                                } else {
                                    message.warningMessage('Przerwane przez uzytkownika');
                                }
                            });
                    } else {
                        throw new Error(`Wersja ${system.release} systemu ${system.distribution} nie jest wspierana!`);
                    }

                } else {
                    throw new Error('Podaj system i jego wersje');
                }
            } else {
                const osInfo = lsbRelease();

                if (!_.isNull(osInfo)) {
                    const system = {distribution: osInfo.distributorID, release: osInfo.release};
                    if (_.isUndefined(argv.sudo)) {
                        system.sudo = '';
                    } else {
                        system.sudo = 'sudo ';
                    }

                    if (argv.system && argv.release) {
                        const system = {distribution: argv.system, release: argv.release.toString()};
                        if (install.findInstallSchema(system.distribution.toLowerCase(), system.release.split('.')[0])) {
                            message.warningMessage(`System: ${system.distribution} w wersji: ${system.release.split('.')[0]}`);
                            new Confirm('Rozpoczac instalacje?')
                                .ask(function (answer) {
                                    if (answer) {
                                        if (_.isUndefined(argv.sudo)) {
                                            system.sudo = '';
                                        } else {
                                            system.sudo = 'sudo';
                                        }
                                        message.infoMessage('Rozpoczynam instalacje!');
                                        install.runInstallation(system.distribution.toLowerCase(), system.release.split('.')[0], system.sudo)
                                    } else {
                                        message.warningMessage('Przerwane przez uzytkownika');
                                    }
                                });
                        } else {
                            throw new Error(`Wersja ${system.release} systemu ${system.distribution} nie jest wspierana!`);
                        }
                    } else {
                        throw new Error(`Wersja ${system.release} systemu ${system.distribution} nie jest wspierana!`);
                    }


                } else {

                    message.errorMessage('Nie mozna zdefiniowac systemu!');
                    message.errorMessage('Sprawdz dokumentacje w celu rozwiazania problemu!');
                }
            }


        }
        else {

            message.errorMessage('Aplikacja nie jest zainstalowana!');
            message.errorMessage('Wykonaj instalacje.');
        }
    }
}).catch((e) => {
    message.errorMessage(e);
});


