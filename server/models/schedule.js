const request = require('axios');
const execa = require('execa');
const download = require('download');

const bot = require('./bot');
const messages = require('./messages');

let intervals = [];

const youTubeDlUpdater = async () => {
    clearTimeout(intervals.ytdl);
    try {
        const youtubedl = await execa.shell('youtube-dl -U');
        if (youtubedl.stdout.includes('youtube-dl is up-to-date')) {
        } else if (youtubedl.stdout.includes('Restart youtube-dl to use the new version.')) {
            messages.infoMessage('Successfully updated yt-dl');
        } else if (youtubedl.stdout.includes('It looks like you installed youtube-dl with a package manager')) {
            messages.errorMessage("Can't update yt-dl installed by apt install or pip. Please remove this installation.");
        } else {
            console.log(youtubedl);
        }
    } catch (e) {
        if (e.stderr.includes('youtube-dl: not found')) {
            try {
                await download('https://yt-dl.org/downloads/latest/youtube-dl', '/usr/local/bin/', {filename: 'youtube-dl'});
                await execa.shell('chmod 777 youtube-dl', {cwd: '/usr/local/bin/'});
                messages.infoMessage('YT-DL was missing but now is installed');
            } catch (e) {
                if (e.code === 'EACCES') {
                    messages.errorMessage('No permission to install yt-dl in system');
                } else {
                    console.log(e);
                }
            }
        } else if (e.stderr.includes('youtube-dl: Permission denied')) {
            try {
                await execa.shell('chmod 777 youtube-dl', {cwd: '/usr/local/bin/'});
                messages.infoMessage('Set missing rights to yt-dl');
            } catch (e) {
                if (e.stderr.includes('Operation not permitted')) {
                    messages.errorMessage('No permission to change yt-dl chmod. Change it by hand or rerun process with root rights');
                } else {
                    console.log(e);
                }
            }
        } else {
            console.log(e, "dupa")
        }
    }
    intervals.ytdl = setInterval(youTubeDlUpdater, 600000);
};

const botAntiCrash = async () => {
    clearTimeout(intervals.anticrash);
    try {
        const bots = await bot.db.findAllBots();
        for (const botData of bots) {
            try {
                const botStatus = await request(`http://127.0.0.1:8080/api/v1/bots/${botData.id}/status`);
                if (botStatus.data.status === 'Offline') {
                    try {
                        await request(`http://127.0.0.1:8080/api/v1/bots/${botData.id}/start`);
                        messages.infoMessage(`Uruchamiam bota o ID: ${botData.id}`);
                    } catch (e) {
                        messages.errorMessage(`Blad podczas uruchamiania bota o ID: ${botData.id}` + e);
                    }
                } else if (botStatus.data.status === 'Disconnected') {
                    try {
                        await request(`http://127.0.0.1:8080/api/v1/bots/${botData.id}/restart`);
                        messages.infoMessage(`Restart bota o ID: ${botData.id}`);
                    } catch (e) {
                        messages.errorMessage(`Blad podczas restartu bota o ID: ${botData.id}`);
                    }
                } else {

                }
            }
            catch (e) {
                if (e.code === "ECONNRESET") {
                    await bot.process.stop(botData.id);
                    messages.errorMessage(`Bot process id: ${botData.id} stuck. Killing...`);
                } else {
                    messages.errorMessage(`Unable to get information about bot id: ${botData.id}`);
                    console.log(e, e.code);
                }
            }
        }
    } catch (e) {
        console.log(e);
        throw new Error('Unable to fetch database');
    }
    intervals.anticrash = setInterval(botAntiCrash, 60000);
};


(() => {
    // youTubeDlUpdater().catch();
    // botAntiCrash().catch();
    intervals.ytdl = setInterval(youTubeDlUpdater, 600000);
    intervals.anticrash = setInterval(botAntiCrash, 60000);
})();
