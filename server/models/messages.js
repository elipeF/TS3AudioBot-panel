const color = require('chalk');
const moment = require('moment');

const init = () => {
    console.log('\n');
    warningMessage('-----------------------------------------', time = false);
    infoMessage('TS3Audiobot -- Panel(SAMO API POKI CO OK)', time = false);
    infoMessage(`version: ${require('./../version').version}`, time = false);
    infoMessage('author: Mateusz Budaj', time = false);
    infoMessage('web: https://elipef.pro', time = false);
    infoMessage('project page: <url>', time = false);
    warningMessage('-----------------------------------------', time = false);
    console.log('\n');
};

const infoMessage = (message, time = true) => {
    if (time === true) {
        console.log(color.bold.blue('( ' + (moment().format('DD.MM.YYYY, HH:mm:ss')) + ' ) ' + message));
    } else {
        console.log(color.bold.blue(message));
    }
};
const warningMessage = (message, time = true) => {
    const orange = color.bold.keyword('orange');
    if (time === true) {
        console.log(orange('( ' + (moment().format('DD.MM.YYYY, HH:mm:ss')) + ' ) ' + message));
    } else {
        console.log(orange(message));
    }
};

const errorMessage = (message, time = true) => {
    if (time === true) {
        console.log(color.bold.red('( ' + (moment().format('DD.MM.YYYY, HH:mm:ss')) + ' ) ' + message));
    } else {
        console.log(color.bold.red(message));

    }
};


module.exports = {errorMessage, infoMessage, warningMessage, init};