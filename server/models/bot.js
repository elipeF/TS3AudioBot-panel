const exist = require('path-exists');
const pkgDir = require('pkg-dir');
const Sequelize = require('sequelize');
const request = require('axios');
const uniqid = require('uniqid');
const validator = require('validator');
const download = require('download');
const decompress = require('decompress');
const execa = require('execa');
const fs = require('fs');
const fse = require('fs-extra');
const _ = require('lodash');
const detect = require('detect-port');
const find = require('find-process');
const messages = require('./messages');

const runningBots = {};

const toJSON = require('./../parsers/toJSON');
const toTOML = require('./../parsers/toTOML');

const {database} = require('./../db/sqlite');
const botFs = {};
const botProcess = {};
const api = {};

const db = database.define('bots', {
    id: {
        type: Sequelize.STRING,
        primaryKey: true
    },
    port: {
        type: Sequelize.INTEGER
    },
    version: {
        type: Sequelize.STRING
    },
    pid: {
        type: Sequelize.INTEGER
    },
});

db.findAllBots = async () => {
    const bots = await db.findAll();
    const botReturn = [];
    for (const bot of bots) {
        botReturn.push(await _.pick(bot, ['id', 'port', 'version']));
    }
    return botReturn;
};

db.findOneBots = async (id) => {
    const bots = await db.findOne({where: {id}});
    return await _.pick(bots.dataValues, ['id', 'port', 'version']);
};


database.sync();

const checkBotInstallPath = async () => {
    try {
        const dir = await pkgDir(__dirname) + '/instances';
        if (await exist(dir)) {
            return true;
        } else {
            await fse.ensureDir(dir)
        }
    } catch (e) {
        throw new Error('Brak uprawnien do zapisu/odczytu');
    }
};

api.restart = async port => {
    let botDefault;
    try {
        const botList = await request(`http://127.0.0.1:${port}/api/bot/list`);
        for (const bot of botList.data) {
            if (typeof botDefault === "undefined") {
                botDefault = bot;
            } else {
                try {
                    await request(`http://127.0.0.1:${port}/api/bot/use/${bot.Id}(/bot/disconnect`);
                } catch (e) {
                    console.log(e);
                }
            }
        }
        if (botDefault) {
            await request(`http://127.0.0.1:${port}/api/bot/use/${botDefault.Id}(/bot/disconnect`);
            await request(`http://127.0.0.1:${port}/api/bot/connect/template/default`);
        } else {
            await request(`http://127.0.0.1:${port}/api/bot/connect/template/default`);
        }
    } catch (e) {
        throw new Error('Blad podczas restartu bota!');
    }
};

api.reload = async port => {
    try {
        await request(`http://127.0.0.1:${port}/api/rights/reload`);
        return true;
    } catch (e) {
        throw new Error('Blad');
    }
};

api.getName = async (id, port) => {
    try {
        if (typeof runningBots[id].name !== "undefined") {
            return runningBots[id].name;
        } else {
            try {
                const name = await request(`http://127.0.0.1:${port}/api/settings/global/get/bot.connect.name`);
                runningBots[id].name = name.data;
                return name.data;
            } catch (e) {
                throw new Error('Blad podczas pobierania nazwy z API' + e);
            }
        }
    } catch (e) {
        throw new Error('Blad podczas pobierania nazwy' + e);
    }
};

api.setName = async (id, port, name) => {
    if (!name && !validator.isLength(name, {min: 3, max: 30})) {
        throw new Error('Bledny nick');
    }
    try {
        runningBots[id].name = name;
        await request(`http://127.0.0.1:${port}/api/settings/global/set/bot.connect.name/${encodeURIComponent(name)}`);
        try {
            await request(`http://127.0.0.1:${port}/api/bot/use/0(/bot/name/${encodeURIComponent(name)}`);
        } catch (e) {
            //TODO LOG WARNING
            console.log(e);
            throw new Error(e);
        }
    } catch (e) {
        throw new Error('Blad podczas ustawiania nazwy' + e);
    }
};

api.getChannel = async (id, port) => {
    try {
        if (typeof runningBots[id].cid !== "undefined") {
            return runningBots[id].cid;
        } else {
            const name = await request(`http://127.0.0.1:${port}/api/settings/global/get/bot.connect.channel`);
            runningBots[id].cid = parseInt(await _.trimStart(name.data, '/'));
            return await _.trimStart(name.data, '/');
        }
    } catch (e) {
        console.log(e);
        throw new Error('Blad podczas pobierania kanalu' + e);
    }
};

api.setChannel = async (id, port, cid) => {
    if (!Number(cid)) {
        throw new Error('Kanal musi byc cyfra');
    } else {
        cid = Number(cid);
    }
    try {
        runningBots[id].cid = cid;
        await request(`http://127.0.0.1:${port}/api/settings/global/set/bot.connect.channel/${encodeURIComponent("/" + cid)}`);
        try {
            await request(`http://127.0.0.1:${port}/api/bot/use/0(/bot/move/${cid}`);
        } catch (e) {
            if (e.response.data.ErrorMessage === 'Cannot move there. (Unknown Teamspeak Error. (already member of channel))') {
                messages.warningMessage(`Bot: ${id} znajduje sie juz na kanale ${cid}`);
            } else {
                console.log(e);
                throw new Error(e);
            }
        }
    } catch (e) {
        console.log(e);
        throw new Error('Blad poczas ustawiania kanalu' + e);
    }
};


api.getYoutubeDl = async port => {
    try {
        const ytdl = await request(`http://127.0.0.1:${port}/api/settings/global/get/tools.youtube-dl.path`);
        if (ytdl.data === '../../lib/youtube-dl') {
            return true;
        } else {
            return false;
        }
    } catch (e) {
        throw new Error('Blad podczas pobierania kanalu');
    }
};

api.setYoutubeDl = async port => {
    try {
        await request(`http://127.0.0.1:${port}/api/settings/global/set/tools.youtube-dl.path/${encodeURIComponent('youtube-dl')}`);
    } catch (e) {
        throw new Error('Blad podczas pobierania kanalu');
    }
};

api.delYoutubeDl = async port => {
    try {
        await request(`http://127.0.0.1:${port}/api/settings/global/set/tools.youtube-dl.path/${encodeURIComponent(' ')}`);
    } catch (e) {
        throw new Error('Blad podczas pobierania kanalu');
    }
};


botProcess.start = async (id, dir) => {
    try {
        await execa.shell(`screen -dmS ${id} mono TS3AudioBot.exe`, {cwd: dir});
        const {stdout} = await execa.shell(`screen -ls | awk '/\\.${id}\\t/ {print strtonum($1)}'`);
        const firstPid = stdout.split('\n');
        const pid = firstPid[firstPid.length - 1].split('.');
        await db.update({pid: pid[pid.length - 1]}, {where: {id}});
        runningBots[id] = {pid};
        return true;
    } catch (e) {
        throw new Error('Bot nie moze wystartowac' + e)
    }
};


botProcess.stop = async (id, port = null) => {
    try {
        if(!port) {
            const {stdout} = await execa.shell(`screen -X -S ${id} quit`);
            if (stdout === '') {
                delete runningBots[id];
            }
        } else {
            try {
                await request(`http://127.0.0.1:${port}/api/quit`);
                delete runningBots[id];
            } catch (e) {
                const {stdout} = await execa.shell(`screen -X -S ${id} quit`);
                if (stdout === '') {
                    delete runningBots[id];
                }
            }
        }
    } catch (e) {
        throw new Error('Nie mozna zastopowac procesu!');
    }
};

botProcess.restart = async (id, dir, port) => {
    try {
        await botProcess.stop(id, port);
        await botProcess.start(id, dir);
        return true;
    }
    catch (e) {
        throw new Error('Nie mozna zresetowac bota' + e);
    }

};

botProcess.running = async (id, port, pid) => {
    try {
        if (runningBots[id]) {
            try {
                const bots = await request(`http://127.0.0.1:${port}/api/bot/list`);
                if (bots.data.length > 0) {
                    for (const bot of bots.data) {
                        if (bot.Id === 0) {
                            try {
                                await request(`http://127.0.0.1:${port}/api/bot/use/${bot.Id}(/bot/info/client`);
                                return {port, pid, status: 3};
                            } catch (e) {
                        /*        if (e.response.data.ErrorMessage === "Unknown Teamspeak Error. (Connection closed)") {
                                    messages.warningMessage(`Bot(${id}) utracil polaczenie z serwerem`)
                                }*/
                                return {port, pid, status: 2}
                            }
                        } else {
                            await request(`http://127.0.0.1:${port}/api/bot/use/${bot.Id}(/bot/disconnect`);
                        }
                    }
                } else {
                    return {port, pid, status: 2};
                }
            } catch (e) {
                return {port: null, pid: null, status: 1};
            }
        } else {
            try {
                const findProcess = await find('pid', pid);
                //console.log(typeof findProcess[0].cmd);
                if (typeof findProcess[0] !== "undefined" && typeof findProcess[0].cmd !== "undefined" && findProcess[0].cmd === `SCREEN -dmS ${id} mono TS3AudioBot.exe`) {
                    runningBots[id] = {pid, port};
                    try {
                        const bots = await request(`http://127.0.0.1:${port}/api/bot/list`);
                        if (bots.data.length > 0) {
                            for (const bot of bots.data) {
                                if (bot.Id === 0) {
                                    try {
                                        await request(`http://127.0.0.1:${port}/api/bot/use/${bot.Id}(/bot/info/client`);
                                        return {port, pid, status: 3};
                                    } catch (e) {
                                /*        if (e.response.data.ErrorMessage === "Unknown Teamspeak Error. (Connection closed)") {
                                            messages.warningMessage(`Bot(${id}) utracil polaczenie z serwerem`)
                                        }*/
                                        return {port, pid, status: 2}
                                    }
                                } else {
                                    await request(`http://127.0.0.1:${port}/api/bot/use/${bot.Id}(/bot/disconnect`);
                                }
                            }
                        } else {
                            return {port, pid, status: 2};
                        }
                    } catch (e) {
                        return {port: null, pid: null, status: 1};
                    }
                } else {
                    try {
                        await db.update({pid: null}, {where: {id}});
                        delete runningBots[id];
                        return {port: null, pid: null, status: 1};
                    } catch (e) {
                        throw new Error('Blad zapytania do bazy danych');
                    }
                }
            } catch (e) {
                throw new Error('Blad podczas lokalizacji' + e )
            }
        }
    } catch (e) {
        throw new Error('Unexpected error when checking bot status.' + e);
    }
};


botFs.usersList = async dir => {
    try {
        if (await !exist(dir + '/rights.toml')) {
            throw new Error('Nie moge wczytac pliku z uprawnieniami');
        }
        const botRights = await toJSON(await fse.readFile(dir + '/rights.toml', 'utf8'));
        return botRights.rule[botRights.rule.length - 1].useruid;
    } catch (e) {
        throw new Error('Blad podczas generowania listy uzytkownikow');
    }
};

botFs.usersSave = async (dir, userUniq) => {
    try {
        if (userUniq && await _.isString(userUniq) && await _.trim(userUniq).length > 2) {
            userUniq = await _.trim(userUniq);
        } else {
            throw new Error('Niepoprawny userUniqueId');
        }
        if (await !exist(dir + '/rights.toml')) {
            throw new Error('Nie moge wczytac pliku z uprawnieniami');
        }
        const botRights = await toJSON(await fse.readFile(dir + '/rights.toml', 'utf8'));
        const userArray = botRights.rule[botRights.rule.length - 1].useruid;
        if (!await _.includes(userArray, userUniq)) {
            await userArray.push(userUniq);
        }
        await fse.outputFile(dir + '/rights.toml', await toTOML(botRights));
        return userArray;
    } catch (e) {
        throw new Error('Blad podczas dodawania uzytkownika');
    }
};

botFs.usersRemove = async (dir, userUniq) => {
    try {
        if (await !exist(dir + '/rights.toml')) {
            throw new Error('Nie moge wczytac pliku z uprawnieniami');
        }
        const botRights = await toJSON(await fse.readFile(dir + '/rights.toml', 'utf8'));
        const userArray = botRights.rule[botRights.rule.length - 1].useruid;
        if (await _.includes(userArray, userUniq)) {
            await _.pull(userArray, userUniq);
        }
        await fse.outputFile(dir + '/rights.toml', await toTOML(botRights));
        return userArray;
    } catch (e) {
        throw new Error('Blad podczas usuwania uzytkownika' + e);
    }
};

botFs.destroy = async id => {
    try {
        const query = await db.destroy({where: {id}});
        if (query === 1) {
            const dir = await pkgDir(__dirname) + `/instances/${id}`;
            if (await exist(dir)) {
                await fse.remove(dir);
            } else {
                throw new Error('Bot juz usuniety');
            }
        } else {
            throw new Error('Bot juz usuniety');
        }
    } catch (e) {
        throw new Error('Blad podczas usuwania');
    }
};

botFs.create = async options => {
    let botDir;
    let botSettings;
    let webApiPort;
    let uniq;
    try {
        const versionCheck = await request('https://elipef.pro/TS3AudioBot/latest/stable/check');
        if (versionCheck.status !== 200) {
            throw new Error('Nie moge zweryfikowac wersji')
        }

        if (await exist(`/tmp/TS3AudioPanel/${versionCheck.data.version}.zip`)) {

        } else {
            try {
                await download('https://elipef.pro/TS3AudioBot/latest/stable/', `/tmp/TS3AudioPanel`, {filename: `${versionCheck.data.version}.zip`})
            } catch (e) {
                throw new Error('Nie moge pobrac ostatniej wersji bota!');
            }
        }

        if (await exist(`/tmp/TS3AudioPanel/config/${versionCheck.data.version}.zip`)) {

        } else {
            try {
                await download(`https://elipef.pro/TS3AudioBot/builds/${versionCheck.data.version}/config`, `/tmp/TS3AudioPanel/config`, {filename: `${versionCheck.data.version}.zip`})
            } catch (e) {
                throw new Error('Nie moge pobrac aktualnego configa!');
            }
        }

        try {
            uniq = uniqid();
            botDir = await pkgDir(__dirname) + `/instances/${uniq}`;
            await decompress(`/tmp/TS3AudioPanel/${versionCheck.data.version}.zip`, botDir);
            await decompress(`/tmp/TS3AudioPanel/config/${versionCheck.data.version}.zip`, botDir);


            if (await !exist(botDir + '/ts3audiobot.toml')) {
                throw new Error('Nie moge wczytac pliku konfiguracyjnego bota');
            }

            const botConfig = await toJSON(await fse.readFile(botDir + '/ts3audiobot.toml', 'utf8'));
            botSettings = {
                serverAddress: options.ipAddress ? !validator.isEmpty(options.ipAddress.toString()) ? options.ipAddress : 'localhost' : 'localhost',
                serverPort: options.port ? validator.isPort(options.port) ? ':' + options.port : '' : '',
                serverPassword: options.password ? options.password : '',
                botName: options.nick ? validator.isLength(options.nick, {
                    min: 3,
                    max: 30
                }) ? options.nick : 'TS3AudioBot - Panel' : 'TS3AudioBot - Panel',
                webApiPort: options.webApiPort ? validator.isPort(options.webApiPort) ? await detect(options.webApiPort) : await detect(8100) : await detect(8100),
            };

            botConfig.bot.connect.address = botSettings.serverAddress + botSettings.serverPort;
            botConfig.bot.connect.name = botSettings.botName;
            botConfig.bot.connect.server_password.pw = botSettings.serverPassword;
            botConfig.bots = {default: {run: true}};

            botConfig.web.interface.enabled = false;
            let portToBeChecked = botSettings.webApiPort;
            while (typeof webApiPort === "undefined") {
                try {
                    const findInDbByIp = await db.findOne({where: {port: portToBeChecked}});
                    if (!findInDbByIp) {
                        webApiPort = portToBeChecked;
                    } else {
                        portToBeChecked = await detect()
                    }
                } catch (e) {
                    console.log('Nie mozna utworzyc configu!');
                }
            }
            botConfig.web.port = webApiPort;

            try {
                await db.build({id: uniq, port: webApiPort, version: versionCheck.data.version}).save();
                await fse.outputFile(botDir + '/ts3audiobot.toml', await toTOML(botConfig));
                await fs.ensureDir(`${botDir}/Bots`);
                await execa.shell('touch bot_default.toml', {cwd: `${botDir}/Bots`});
            } catch (e) {
                throw new Error('Nie mozna zapisac do bazy')
            }
        } catch (e) {
            throw new Error('Nie moge rozpakowac w katalogu "instances"!');
        }

        return ({
            id: uniq,
            nickname: botSettings.botName,
            address: botSettings.serverAddress + botSettings.serverPort,
            webApiPort: webApiPort,
        });

    } catch (e) {
        await fse.remove(botDir);
        throw new Error('Nie mozna zainstalowac bota!' + e);
    }
};


module.exports = {db, checkBotInstallPath, fs: botFs, process: botProcess, api};