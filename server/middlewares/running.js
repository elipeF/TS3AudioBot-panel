const moment = require('moment');

const {process} = require('./../models/bot');
const messages = require('./../models/messages');

const cache = {};

const running = async (req, res, next) => {
        try {
            const id = req.params.id;
            if (typeof cache[id] !== "undefined" && cache[id].time === moment().format('DD.MM.YYYY, HH:mm:ss')) {
                if (cache[id].value.status === 200) {
                    if (!req.bot) {
                        req.bot = {};
                    }
                    req.bot.process = cache[id].value.data;
                    next()
                } else {
                    res.status(cache[id].value.status).send(cache[id].value.data);
                }
            } else {
                if (!cache[id]) {
                    cache[id] = {};
                }
                if (!req.bot) {
                    req.bot = {};
                }
                req.bot.process = await process.running(id, req.bot.data.port, req.bot.data.pid);
                if (req.bot.process.status === 3) {
                    cache[id].value = {status: 200, data: req.bot.process};
                    cache[id].time = moment().format('DD.MM.YYYY, HH:mm:ss');
                    next();
                } else if (req.bot.process.status === 2) {
                    //      messages.warningMessage(`Bot(ID: ${id}) poza serwerem`);
                    cache[id].value = {status: 400, data: 'Bot poza serwerem'};
                    cache[id].time = moment().format('DD.MM.YYYY, HH:mm:ss');
                    res.status(400).send('Bot poza serwerem');
                } else if (req.bot.process.status === 1) {
                    //   messages.warningMessage(`Bot(ID: ${id}) wylaczony`);
                    cache[id].value = {status: 400, data: 'Bot wylaczony'};
                    cache[id].time = moment().format('DD.MM.YYYY, HH:mm:ss');
                    res.status(400).send('Bot wylaczony');
                } else {
                    //  console.log(req.bot.process);
                    cache[id].value = {status: 400, data: 'Problem z botem ID: ' + id};
                    cache[id].time = moment().format('DD.MM.YYYY, HH:mm:ss');
                    res.status(400).send('Problem z botem ID: ' + id);
                }
            }
        }
        catch
            (e) {
            console.log(e);
            res.status(400).send('Problem z botem');
        }
    }
;


module.exports = {running};