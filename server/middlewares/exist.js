const exist = require('path-exists');
const pkgDir = require('pkg-dir');
const moment = require('moment');

const bot = require('./../models/bot');

const cache = {};

const exists = async (req, res, next) => {
    try {
        const id = req.params.id;
        if (typeof cache[id] !== "undefined" && cache[id].time === moment().format('DD.MM.YYYY, HH:mm:ss')) {
            if (cache[id].value.status === 200) {
                if (!req.bot) {
                    req.bot = {};
                }
                req.bot.data = cache[id].value.data;
                next()
            } else {
                res.status(cache[id].value.status).send(cache[id].value.data);
            }
        } else {
            const botDb = await bot.db.findOne({where: {id}});
            if (botDb) {
                if (!cache[id]) {
                    cache[id] = {};
                }
                const dir = await pkgDir(__dirname) + `/instances/${id}`;
                if (await exist(dir)) {
                    if (!req.bot) {
                        req.bot = {};
                    }
                    req.bot.data = {dir, port: botDb.dataValues.port, pid: botDb.dataValues.pid, id};
                    cache[id].value = {status: 200, data: req.bot.data};
                    cache[id].time = moment().format('DD.MM.YYYY, HH:mm:ss');
                    next();
                } else {
                    try {
                        await bot.fs.destroy(id);
                        cache[id].value = {status: 400, data: 'Bot usuniety'};
                        cache[id].time = moment().format('DD.MM.YYYY, HH:mm:ss');
                        res.status(400).send('Bot usuniety');
                    } catch (e) {
                        cache[id].value = {status: 400, data: 'Blad podczas usuwania bota'};
                        cache[id].time = moment().format('DD.MM.YYYY, HH:mm:ss');
                        res.status(400).send('Blad podczas usuwania bota');
                    }
                }
            } else {
                cache[id].value = {status: 400, data: 'Brak bota o podanym ID'};
                cache[id].time = moment().format('DD.MM.YYYY, HH:mm:ss');
                res.status(400).send('Brak bota o podanym ID');
            }
        }


    } catch (e) {
        res.status(400).send('Blad bazy danych');
    }
};

module.exports = {exists};