const tomlify = require('./tomlify');


module.exports = async (string) => {
    return await tomlify.toToml(string)
};