const toml = require('toml');

module.exports = async (string) => {
    return await toml.parse(string)
};